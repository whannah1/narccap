load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "~/Research/NARCCAP/NARCCAP.FUNCTIONS.ncl"
begin
	
	;RMODEL = (/"ECP2","HRM3"/)
	;RMODEL = (/"ECP2","HRM3","RCM3"/)
	;RMODEL = (/"CRCM","MM5I","WRFG"/)
	;RMODEL = (/"ECP2"/)
	RMODEL = (/"ECP2","HRM3","RCM3","CRCM","MM5I","WRFG"/)

	var = "pr"      ; Precipitation
	;var = "snm"    ; Snow Melt

	period = "current"		; current / future

	dir = "~/Research/NARCCAP/"

	debug  = False      ;
	regrid = True       ; Use ESMF Regridding routine
	redowt = False       ; Recalculate weights for ESMF Regridding
;===================================================================================
;===================================================================================
num_m = dimsizes(RMODEL)
num_v = dimsizes(var)

DMODEL = new(num_m,string)
DMODEL = where(RMODEL.eq.(/"ECP2"/),"gfdl",DMODEL)
DMODEL = where(RMODEL.eq.(/"HRM3"/),"gfdl",DMODEL)
DMODEL = where(RMODEL.eq.(/"RCM3"/),"gfdl",DMODEL)
DMODEL = where(RMODEL.eq.(/"CRCM"/),"ccsm",DMODEL)
DMODEL = where(RMODEL.eq.(/"MM5I"/),"ccsm",DMODEL)
DMODEL = where(RMODEL.eq.(/"WRFG"/),"ccsm",DMODEL)

idir = "~/Research/NARCCAP/"+DMODEL+"/"+RMODEL+"/"

; Specify limits for loading in the data
; This only helps to reduce the memory burden
; This does not alter the plot bounds
lat1 = 28.
lat2 = 50.
lon1 = 360 - 120.
lon2 = 360 -  95.

; Colorado Region
;lat1 = 37.
;lat2 = 41.
;lon1 = 360 - 109.
;lon2 = 360 - 102.

; map bounds
plat1 = 32.
plat2 = 48.
plon1 = 360 - 118.
plon2 = 360 -  98.
;===================================================================================
; ESMF Regridding options
;===================================================================================
	opt = True
	opt@ForceOverwrite          = redowt
	opt@Debug                   = debug
	opt@DstGridType             = "0.25deg" 
	opt@DstLLCorner             = (/ lat1, lon1 /)
	opt@DstURCorner             = (/ lat2, lon2 /)
	opt@SrcRegional             = True
	opt@DstRegional             = True
	opt@InterpMethod            = "bilinear"       ; Default is bilinear
;===================================================================================
; Make year-month index
;===================================================================================
	days_per_month = (/0,31,28,31,30,31,30,31,31,30,31,30,31/)
	ntpd = 8
	nyr  = 10
	yrmn = new(nyr*365*ntpd,integer)
	do m = 0,11
	do y = 0,nyr-1
		t1 = sum(days_per_month(:m  ))*ntpd + y*365*ntpd
		t2 = sum(days_per_month(:m+1))*ntpd + y*365*ntpd -1
  		yrmn(t1:t2) = y*100+m
  	end do
	end do
;===================================================================================
; Load Data
;===================================================================================
do m = 0,num_m-1
;do p = 0,1
	;if p.eq.0 then yr = (/1991,1996/) end if
	;if p.eq.1 then yr = (/2061,2066/) end if
	if period.eq."current" then yr = (/1991,1996/) end if
	if period.eq."future"  then yr = (/2061,2066/) end if
	topt = opt
	RM = RMODEL(m)
	DM = DMODEL(m)
	infile1 = addfile(idir(m)+var+"_"+RM+"_"+DM+"_"+yr(0)+"010103.nc","r")
	infile2 = addfile(idir(m)+var+"_"+RM+"_"+DM+"_"+yr(1)+"010103.nc","r")
	print("m = "+m+"   "+RM+"  ("+DM+")")
	;---------------------------------------
	; Load Data
	;---------------------------------------
	if debug then
		;print(infile1)
		print("--------------------------------------------")
		print("case: "+RMODEL(m))
		print("  nt 1 "+dimsizes(infile1->time))
		print("  nt 2 "+dimsizes(infile2->time))
		print("")
	end if
	time1 = infile1->time
	time2 = infile2->time
	lat   = infile1->lat
	lon   = infile1->lon
	box   = getind_latlon2d(lat,lon,(/lat1,lat2/),(/lon1,lon2/))
	yc    = infile1->yc(box(0,0):box(1,0))
	xc    = infile1->xc(box(0,1):box(1,1))
	plat  = lat(box(0,0):box(1,0),box(0,1):box(1,1))
	plon  = lon(box(0,0):box(1,0),box(0,1):box(1,1))
	nt1   = dimsizes(time1)
	nt2   = dimsizes(time2)
	nt    = nt1+nt2
	ny    = dimsizes(yc)
	nx    = dimsizes(xc)
	xdim  = (/nt,ny,nx/)
	X = new(xdim,float)
	
	;if debug then 
	;    t1 = 5
	;    t2 = 6
	;    X(0,:,:) = dim_avg_n_Wrap(infile1->$var$(t1*8:t2*8,box(0,0):box(1,0),box(0,1):box(1,1)),0)
	;else
		X(   :nt1-1,:,:) = infile1->$var$(:,box(0,0):box(1,0),box(0,1):box(1,1))
		X(nt1:nt -1,:,:) = infile2->$var$(:,box(0,0):box(1,0),box(0,1):box(1,1))
	;end if

	X!0 = "time"
	X!1 = "yc"
	X!2 = "xc"
	X&yc = yc
	X&xc = xc
	X@lat2d = plat
	X@lon2d = plon

	if var.eq."pr" then X = (/ X /1000. * (24.*3600.*1000.) /) end if   ; Convert to mm/day
	;---------------------------------------
	; convert to monthly means
	;---------------------------------------


	aX = new((/12*10,ny,nx/),float)

	;printVarSummary(yrmn)
	;printVarSummary(X)

	tyrmn = conform(X,yrmn(:nt-1),0)

	if debug then 
	print("------------------------------------------------------")
	print(dimsizes(X))
	print("------------------------------------------------------")
	print(dimsizes(tyrmn))
	print("------------------------------------------------------")
	end if
	
	do im = 0,11
	do iy = 0,nyr-1
		aX(iy*12+im,:,:) = dim_avg_n( where(tyrmn.eq.(iy*100+im),X,X@_FillValue) ,0)
	end do
	end do

	copy_VarAtts(X,aX)
	aX!0 = "time"
	aX!1 = "yc"
	aX!2 = "xc"
	aX&yc = X&yc
	aX&xc = X&xc

	;printVarSummary(X)
	;printVarSummary(aX)

	delete([/X,time1,time2,tyrmn/])
;end do

	if regrid then
		topt@SrcMask2D      = where(.not.ismissing(aX),1,0)
		wgtfile = dir+"wgts.ESMWF."+RM+".nc"
		if redowt then
			raX = ESMF_regrid(aX,topt)
			twgtfile = dir+"weights_file.nc" 
			system("cp "+twgtfile+" "+wgtfile)
		else
			raX = ESMF_regrid_with_weights (aX, wgtfile, topt)
		end if
	else
		raX = aX
	end if

	delete([/aX,topt,plat,plon,lat,lon,yc,xc/])

	;printVarSummary(raX)

	ofile = idir(m)+"monthly."+var+"_"+RM+"_"+DM+"_"+yr(0)+".nc"
	outfile = addfile(ofile,"c")
	outfile->$var$ = raX

	print("")
	print(" "+ofile)
	print("")   
end do
;===================================================================================
;===================================================================================
end